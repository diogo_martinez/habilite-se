import java.io.FileOutputStream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;


public class TesteColunaItext {
	
	public static void main(String args[]){
		Document document = new Document();
		try {
		    PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("output.pdf"));
		    document.open();
		    PdfContentByte cb = writer.getDirectContent();
		 // we need to switch to text mode
		    cb.beginText();
		    // write text centered at 155 mm from left side and 270 mm from bottom
		    cb.showTextAligned(PdfContentByte.ALIGN_CENTER, "Our headline", 439, 765, 0);
		    // leave text mode
		    cb.endText();
		     
		    // now draw a line below the headline
		    cb.setLineWidth(1f); 
		    cb.moveTo(0, 755);
		    cb.lineTo(595, 755);
		    cb.stroke(); 
		 // first define a standard font for our text
		    Font helvetica8BoldBlue = FontFactory.getFont(FontFactory.HELVETICA, 8f, Font.BOLD, BaseColor.BLUE);
		     
		    // create a column object
		    ColumnText ct = new ColumnText(cb);
		     
		    // define the text to print in the column
		    Phrase myText = new Phrase("Lorem ipsum dolor sit amet, ...", helvetica8BoldBlue);
		    ct.setSimpleColumn(myText, 72, 600, 355, 317, 10, Element.ALIGN_LEFT);
		    ct.go(); 
		 
		   // now we can place content elements on the page
		} catch (Exception e) {
		}
		document.close(); 

		} 


	

}
