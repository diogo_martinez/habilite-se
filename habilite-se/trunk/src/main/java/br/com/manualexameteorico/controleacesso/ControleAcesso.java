package br.com.manualexameteorico.controleacesso;

import java.util.Map;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class ControleAcesso extends AbstractInterceptor {

	private static final long serialVersionUID = 1L;

	public String intercept(ActionInvocation invocation) throws Exception {
		Map<String, Object> session = invocation.getInvocationContext()
				.getSession();

		if (session.get("usuario") == null) {

			return "usuarioNaoLogado";
		} else {

			return invocation.invoke();
		}

	}

}
