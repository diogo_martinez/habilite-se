package br.com.manualexameteorico.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;

public class BemVindoAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	@Action(value = "/BemVindo", results = { @Result(name = "sucesso", location = "/WEB-INF/jsp/index.jsp") })
	public String execute() {
		return "sucesso";
	}

}
