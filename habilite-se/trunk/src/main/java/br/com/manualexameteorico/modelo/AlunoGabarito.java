package br.com.manualexameteorico.modelo;

import java.util.Set;

public class AlunoGabarito {
	
	private Integer id;	
	
	private Set<RespostaAluno> gabarito;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Set<RespostaAluno> getGabarito() {
		return gabarito;
	}

	public void setGabarito(Set<RespostaAluno> gabarito) {
		this.gabarito = gabarito;
	}

}
