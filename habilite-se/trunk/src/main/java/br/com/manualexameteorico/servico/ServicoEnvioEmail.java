package br.com.manualexameteorico.servico;

import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;

import br.com.manualexameteorico.vo.ContatoVO;

@Service("servicoEmail")
public class ServicoEnvioEmail {
	
	
	public void enviaEmail(ContatoVO contatoVO){
		Properties props = new Properties();
	    props.put("mail.smtp.host", "mail.habilite-se.com.br");
	    props.put("mail.smtp.port", "587");
	    props.put("mail.smtp.auth", "true");
	    Authenticator auth = new Authenticator() {
	      public PasswordAuthentication getPasswordAuthentication() {
	        return new PasswordAuthentication("atendimento@habilite-se.com.br", "diogomartinez");
	      }};


	    props.put("mail.from", contatoVO.getEmail());
	    
	    Session session = Session.getInstance(props, auth);

	    try {
	        MimeMessage msg = new MimeMessage(session);
	        msg.setFrom();
	        msg.setRecipients(Message.RecipientType.TO,
	                          "atendimento@habilite-se.com.br");
	        msg.setSubject("Contato via Web de " + contatoVO.getNome()+" " + contatoVO.getTelefone());
	        msg.setSentDate(new Date());
	        msg.setText(contatoVO.getMensagem());
	        Transport.send(msg);
	    } catch (MessagingException mex) {
	        System.out.println("send failed, exception: " + mex);
	    }

	}

}
