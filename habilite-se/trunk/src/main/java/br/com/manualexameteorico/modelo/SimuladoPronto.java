package br.com.manualexameteorico.modelo;

import java.util.Set;

public class SimuladoPronto {
	
	private Integer id;
	
	private Set <SimuladoProntoGabarito> gabarito;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Set<SimuladoProntoGabarito> getGabarito() {
		return gabarito;
	}

	public void setGabarito(Set<SimuladoProntoGabarito> gabarito) {
		this.gabarito = gabarito;
	}	
	

}
