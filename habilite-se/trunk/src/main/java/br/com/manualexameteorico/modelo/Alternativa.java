package br.com.manualexameteorico.modelo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Alternativa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	
	private String descricao;
	
	private Boolean isCerta;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getIsCerta() {
		return isCerta;
	}

	public void setIsCerta(Boolean isCerta) {
		this.isCerta = isCerta;
	}
	

}
