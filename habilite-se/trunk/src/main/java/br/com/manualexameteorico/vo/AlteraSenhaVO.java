package br.com.manualexameteorico.vo;

public class AlteraSenhaVO {

	private String usuario;

	private String senhaAntiga;

	private String novaSenha;

	private String novaSenhaRepetida;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenhaAntiga() {
		return senhaAntiga;
	}

	public void setSenhaAntiga(String senhaAntiga) {
		this.senhaAntiga = senhaAntiga;
	}

	public String getNovaSenha() {
		return novaSenha;
	}

	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}

	public String getNovaSenhaRepetida() {
		return novaSenhaRepetida;
	}

	public void setNovaSenhaRepetida(String novaSenhaRepetida) {
		this.novaSenhaRepetida = novaSenhaRepetida;
	}

}
