package br.com.manualexameteorico.servico.pdf;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.manualexameteorico.modelo.Alternativa;
import br.com.manualexameteorico.modelo.Pergunta;
import br.com.manualexameteorico.servico.ServicoSimuladoImpl;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Service("servicoSimuladoPDF")
public class ServicoSimuladoPDF extends ServicoSimuladoImpl {

	public static final float[][] COLUMNS = { { 10, 26, 300, 780 },
			{ 305, 26, 595, 780 } };

	// o gabarito como lista deve performar mais, mas h� a chance de uma
	// inconsistencia existir no banco e o gabarito ficar� errado.
	// Exemplo: uma das perguntas n�o tem uma alternativa marcada como
	// certa. ela nao entrar� na lista e gerar� erro em todas a seguir.
	List<String> gabarito = null;

	@Override
	public InputStream geraSimulado() {
		gabarito = new ArrayList<String>();
		Document doc = null;
		ByteArrayOutputStream os = null;
		List<Pergunta> perguntas = new ArrayList<Pergunta>();

		try {

			perguntas = getPerguntas();

			doc = new Document(PageSize.A4, 68, 68, 68, 68);

			os = new ByteArrayOutputStream();

			PdfWriter writer = PdfWriter.getInstance(doc, os);
			writer.setBoxSize("art", new Rectangle(36, 20, 559, 804));

			EventoCabecalhoRodape cabe�alhoRodape = new EventoCabecalhoRodape();
			writer.setPageEvent(cabe�alhoRodape);

			doc.open();

			ColumnText ct = new ColumnText(writer.getDirectContent());
			int column = 0;

			ct.setSimpleColumn(COLUMNS[column][0], COLUMNS[column][1],
					COLUMNS[column][2], COLUMNS[column][3]);

			int status = ColumnText.START_COLUMN;
			float y;
			int questao = 1;
			for (Pergunta pergunta : perguntas) {
				y = ct.getYLine();
				addContent(ct, pergunta, true, questao);
				status = ct.go(true);
				if (ColumnText.hasMoreText(status)) {
					column = (column + 1) % 2;
					if (column == 0)
						doc.newPage();

					ct.setSimpleColumn(COLUMNS[column][0], COLUMNS[column][1],
							COLUMNS[column][2], COLUMNS[column][3]);
					y = COLUMNS[column][3];

				}
				ct.setYLine(y);
				ct.setText(null);
				addContent(ct, pergunta, false, questao);
				questao++;
				status = ct.go();
			}
			// doc.newPage();
			//
			// PdfPTable table = new PdfPTable(2);
			//
			// table.setWidthPercentage(35.0f);
			// PdfPCell header = new PdfPCell(new Paragraph("Gabarito"));
			// header.setColspan(2);
			// header.setHorizontalAlignment(Element.ALIGN_CENTER);
			// table.addCell(header);
			// for (int i = 0; i < gabarito.size(); i++) {
			// table.addCell(String.valueOf(i + 1));
			// table.addCell(gabarito.get(i));
			// }
			//
			// doc.add(table);
			
			//####
		//	Chunk geraGabarito = new Chunk();
			for (int i = 0; i < gabarito.size(); i++) {
				System.out.println(i+1 +": " + gabarito.get(i));
		//		geraGabarito.append(i + 1 + "-" + gabarito.get(i) + "; ");
			}
	//		geraGabarito.setFont(new Font(FontFamily.COURIER, 5, Font.BOLD));
	//		Rectangle rect = writer.getBoxSize("art");
	//		ColumnText.showTextAligned(writer.getDirectContent(),
	//				Element.ALIGN_CENTER, new Phrase(geraGabarito),
	//				(rect.getLeft() + rect.getRight()) / 2,
	//				rect.getBottom() - 18, 0);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			doc.close();
		}
		return new ByteArrayInputStream(os.toByteArray());

	}

	/**
	 * Adds info about a movie in a ColumnText object
	 * 
	 * @param ct
	 *            A ColumnText object
	 * @param pergunta
	 *            A Movie POJO
	 * @throws IOException
	 * @throws MalformedURLException
	 * @throws BadElementException
	 */
	private void addContent(ColumnText ct, Pergunta pergunta,
			Boolean isSimulate, int numeroQuestao) throws BadElementException,
			MalformedURLException, IOException {
		PdfPTable table = null;
		Image imagem = null;
		Font f = FontFactory.getFont("Times-Roman", 10, Font.BOLD);
		PdfPCell enunciado = new PdfPCell();

		if (pergunta.getImagem() != null && !pergunta.getImagem().equals("")) {
			// TODO retirar esse hardcoded
			float colunas[] = { 234, 55 };
			table = new PdfPTable(colunas);
			// TODO retirar esse hardcoded
		//	imagem = Image.getInstance(String.format("/imagens/placas/%s",
		//			pergunta.getImagem().trim()));
			
			imagem = Image.getInstance(String.format("/home/habilite/imagens/placas/%s",
					pergunta.getImagem().trim()));
			imagem.scaleToFit(50, 50);
			// imagem.setAlignment(Element.ALIGN_RIGHT);
			enunciado.setColspan(2);

		} else {
			table = new PdfPTable(1);
		}
		table.setWidthPercentage(100.0f);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase p1 = new Phrase(numeroQuestao + ")" + " "
				+ pergunta.getDescricao(), f);
		p1.setLeading(10);
		//
		//
		//
		// //header.setHorizontalAlignment(Element.ALIGN_CENTER);
		enunciado.addElement(p1);
		enunciado.setBorder(Rectangle.NO_BORDER);
		table.addCell(enunciado);

		Chunk alternativas = new Chunk();
		int i = 0;
		for (Alternativa alternativa : pergunta.getAlternativas()) {

			String marcador = getMarcador(i);
			// Alternativa alternativa = pergunta.getAlternativas().get(i);

			alternativas.append(marcador + ")" + " "
					+ alternativa.getDescricao() + "\n");

			if (alternativa.getIsCerta() && !isSimulate) {
				gabarito.add(marcador);
			}

			i++;
		}
		Font fontAlternativas = FontFactory.getFont("Times-Roman", 10);
		alternativas.setFont(fontAlternativas);
		PdfPCell cellAlternativas = new PdfPCell(new Phrase(alternativas));
		cellAlternativas.setBorder(Rectangle.NO_BORDER);

		table.addCell(cellAlternativas);
		if (imagem != null) {

			PdfPCell cellImagem = new PdfPCell(imagem);
			cellImagem.setBorder(Rectangle.NO_BORDER);
			cellImagem.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(cellImagem);

		}

		ct.addElement(table);
	}

	private String getMarcador(int i) {
		if (i == 0)
			return "a";
		else if (i == 1)
			return "b";
		else if (i == 2)
			return "c";
		else
			return "d";

	}
}