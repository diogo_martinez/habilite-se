package br.com.manualexameteorico.action;

import java.util.Map;

import javax.persistence.NoResultException;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.manualexameteorico.modelo.Usuario;
import br.com.manualexameteorico.servico.interfaces.InterfaceServicoUsuario;
import br.com.manualexameteorico.vo.AlteraSenhaVO;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;

public class LoginAction extends ActionSupport implements SessionAware {

	private static final long serialVersionUID = 1L;

	private Map<String, Object> session;

	@Autowired
	private InterfaceServicoUsuario servicoUsuario;

	private Usuario usuario;
	private AlteraSenhaVO alteraSenhaVO;

	@Action(value = "/NovoUsuario", results = {
			@Result(name = "sucesso", type = "redirectAction", params = {
					"actionName", "BemVindo" }),
			@Result(name = "erro", location = "/jsp/login/erro.jsp"),
			@Result(name = "erroSistema", location = "/WEB-INF/jsp/erro/erroSistema.jsp") })
	public String inserirUsuario() {

		try {
			servicoUsuario.novoUsuario(usuario);
			addActionMessage("novo.usuario.sucesso");
			return "sucesso";
		} catch (Exception e) {
			addActionError(e.getMessage());
			return "erroSistema";
		}

	}

	@Validations(requiredStrings = {
			@RequiredStringValidator(type = ValidatorType.SIMPLE, fieldName = "usuario.usuario", key = "usuario.vazio"),
			@RequiredStringValidator(type = ValidatorType.SIMPLE, fieldName = "usuario.senha", key = "senha.vazio") })
	@Action(value = "/Login", results = {
			@Result(name = "sucesso", location = "/WEB-INF/jsp/simulado/inicial.jsp"),
			@Result(name = "input", location = "/WEB-INF/jsp/index.jsp"),
			@Result(name = "erroSistema", location = "/WEB-INF/jsp/erro/erroSistema.jsp") })
	public String login() {

		try {

			usuario = servicoUsuario.buscarUsuarioSenha(usuario);

		} catch (NoResultException e) {
			addFieldError("usuario.usuario", getText("usuario.vazio"));
			return "input";
		} catch (Exception e) {
			addActionError(e.getMessage());
			return "erroSistema";
		}

		session.put("usuario", usuario);
		return "sucesso";

	}

	@SkipValidation
	@Action(value = "/logout", results = { @Result(name = "sucesso", type = "redirectAction", params = {
			"actionName", "BemVindo" }) })
	public String logout() {

		session.put("usuario", null);
		return "sucesso";
	}

	@SkipValidation
	@Action(value = "/IniciaAlteracaoSenha", results = { @Result(name = "sucesso", location = "/WEB-INF/jsp/login/alterarSenha.jsp") })
	public String iniciaAlteracaoSenha() {
		return "sucesso";
	}

	@Action(value = "/AlteraSenha", results = {
			@Result(name = "sucesso", type = "redirectAction", params = {
					"actionName", "BemVindo" }),
			@Result(name = "erro", location = "/WEB-INF/jsp/login/alterarSenha.jsp"),
			@Result(name = "erroSistema", location = "/WEB-INF/jsp/erro/erroSistema.jsp") })
	public String alteraSenha() {

		if (!alteraSenhaVO.getNovaSenha().equals(
				alteraSenhaVO.getNovaSenhaRepetida())) {
			addActionError("senha.naoconfere");
			return "erro";
		}

		Usuario usuario = new Usuario();

		usuario.setUsuario(alteraSenhaVO.getUsuario());
		usuario.setSenha(alteraSenhaVO.getSenhaAntiga());
		try {
			usuario = servicoUsuario.buscarUsuarioSenha(usuario);
			usuario.setSenha(alteraSenhaVO.getNovaSenha());
			servicoUsuario.atualizaUsuario(usuario);

		} catch (NoResultException e) {
			addActionError("senha.antiga.invalida");
			return "erro";
		} catch (Exception e) {
			addActionError(e.getMessage());
			return "erroSistema";
		}
		addActionMessage("senha.alterada.sucesso");
		return "sucesso";
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;

	}

	public AlteraSenhaVO getAlteraSenhaVO() {
		return alteraSenhaVO;
	}

	public void setAlteraSenhaVO(AlteraSenhaVO alteraSenhaVO) {
		this.alteraSenhaVO = alteraSenhaVO;
	}

}
