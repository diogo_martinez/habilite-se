package br.com.manualexameteorico.servico.interfaces;

import br.com.manualexameteorico.modelo.Usuario;

public interface InterfaceServicoUsuario {

	public Boolean isCodigoAcessoValido(String codigoAcesso);

	public Usuario buscarUsuarioSenha(Usuario usuario) throws Exception;

	public void novoUsuario(Usuario usuario) throws Exception;

	public void atualizaUsuario(Usuario usuario) throws Exception;

}
