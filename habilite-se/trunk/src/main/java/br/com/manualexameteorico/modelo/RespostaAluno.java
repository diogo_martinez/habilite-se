package br.com.manualexameteorico.modelo;

public class RespostaAluno {

	
	private Integer id;
	
	private AlunoGabarito idAlunoGabarito;
	
	private Integer num_pergunta;
	
	private Integer num_resposta;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public AlunoGabarito getIdAlunoGabarito() {
		return idAlunoGabarito;
	}

	public void setIdAlunoGabarito(AlunoGabarito idAlunoGabarito) {
		this.idAlunoGabarito = idAlunoGabarito;
	}

	public Integer getNum_pergunta() {
		return num_pergunta;
	}

	public void setNum_pergunta(Integer num_pergunta) {
		this.num_pergunta = num_pergunta;
	}

	public Integer getNum_resposta() {
		return num_resposta;
	}

	public void setNum_resposta(Integer num_resposta) {
		this.num_resposta = num_resposta;
	}
}
