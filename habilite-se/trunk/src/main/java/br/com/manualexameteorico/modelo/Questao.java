package br.com.manualexameteorico.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@NamedQuery(name = "listaTodasQuestoes", query = "select q from Questao q order by q.id")
public class Questao implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;	
	

	@ManyToMany(cascade = CascadeType.ALL,fetch=FetchType.EAGER)	
	@JoinTable(name = "pergunta_questao", joinColumns = { @JoinColumn(name = "questaoId") }, inverseJoinColumns = { @JoinColumn(name = "perguntaId") })
	private List<Pergunta> perguntas;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	

	public List<Pergunta> getPerguntas() {
		return perguntas;
	}

	public void setPerguntas(List<Pergunta> perguntas) {
		this.perguntas = perguntas;
	}
}
