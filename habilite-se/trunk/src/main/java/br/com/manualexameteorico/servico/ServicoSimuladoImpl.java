package br.com.manualexameteorico.servico;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.manualexameteorico.modelo.Pergunta;
import br.com.manualexameteorico.modelo.Questao;
import br.com.manualexameteorico.servico.interfaces.InterfaceServicoSimulado;

public abstract class ServicoSimuladoImpl implements InterfaceServicoSimulado {

	@PersistenceContext
	private EntityManager emf;

	private static List<Questao> questoes = new ArrayList<Questao>();

	protected List<Pergunta> getPerguntas() {
		try {

			synchronized (this.questoes) {

				if (ServicoSimuladoImpl.questoes.isEmpty()) {

					Query query = emf.createNamedQuery("listaTodasQuestoes");
					List<Questao> questoes = (List<Questao>) query
							.getResultList();
					this.questoes = questoes;

				}

			}
			return getPerguntasAleatorias();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// private Map<Integer, List<Pergunta>> organizaMapaPorNumeroQuestao(
	// List<Pergunta> perguntas) {

	// for (Pergunta pergunta : perguntas) {
	// if (mapaQuestoes.containsKey(pergunta.getIdQuestao().getId())) {
	// List<Pergunta> aux = mapaQuestoes.get(pergunta.getIdQuestao()
	// .getId());
	// aux.add(pergunta);
	// } else {
	// List<Pergunta> aux = new ArrayList<Pergunta>();
	// aux.add(pergunta);
	// mapaQuestoes.put(pergunta.getIdQuestao().getId(), aux);
	// }
	// }
	// return mapaQuestoes;
	// }

	private List<Pergunta> getPerguntasAleatorias() {
		List<Pergunta> perguntasAleatorias = new ArrayList<Pergunta>();

		for (Questao questao : this.questoes) {

			Pergunta perguntaAux = null;

			while (perguntaAux == null) {
				int i = 1 + (int) (Math.random() * questao.getPerguntas()
						.size());
				
				perguntaAux = questao.getPerguntas().get(i - 1);
				for (Pergunta pergunta : perguntasAleatorias) {
					if (pergunta.getId() == perguntaAux.getId()) {
						perguntaAux = null;
						break;
					}

				}
			}
			perguntasAleatorias.add(perguntaAux);

		}

		return perguntasAleatorias;
	}

}
