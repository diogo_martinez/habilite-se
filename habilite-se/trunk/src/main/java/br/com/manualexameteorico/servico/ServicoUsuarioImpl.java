package br.com.manualexameteorico.servico;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.manualexameteorico.modelo.CodigoAcesso;
import br.com.manualexameteorico.modelo.Usuario;
import br.com.manualexameteorico.servico.interfaces.InterfaceServicoUsuario;

@Service("servicoUsuario")
public class ServicoUsuarioImpl implements InterfaceServicoUsuario {

	@PersistenceContext
	EntityManager emf;

	public Boolean isCodigoAcessoValido(String codigoAcesso) {

		try {

			CodigoAcesso codigoAcessoAux = buscaCodigoAcesso(codigoAcesso);

			if (codigoAcessoAux != null && codigoAcessoAux.getAtivo()) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	private CodigoAcesso buscaCodigoAcesso(String codigoAcesso)
			throws Exception {
		Query query = emf.createNamedQuery("buscaCodigoAcesso",
				CodigoAcesso.class).setParameter("codigoAcesso", codigoAcesso);
		try {
			return (CodigoAcesso) query.getSingleResult();
		} catch (Exception e) {

			throw new Exception(e);
		}
	}

	@Transactional
	public void novoUsuario(Usuario usuario) throws Exception {
		try {

			CodigoAcesso codigo = buscaCodigoAcesso(usuario.getCodigoAcesso()
					.getCodigoAcesso());
			codigo.setAtivo(false);

			usuario.setCodigoAcesso(codigo);
			usuario.setTipoUsuario(codigo.getTipoUsuario());
			usuario.setIdPessoaJuridica(codigo.getIdPessoaJuridica());

			emf.persist(usuario);

		} catch (Exception e) {
			throw new Exception(e);
		}

	}

	public Usuario buscarUsuarioSenha(Usuario usuario) throws Exception {
		Query query = emf.createNamedQuery("consultaLoginSenha");
		query.setParameter("usuario", usuario.getUsuario());
		query.setParameter("senha", usuario.getSenha());
		try {

			return (Usuario) query.getSingleResult();

		} catch (NoResultException e) {
			throw new NoResultException();
		} catch (Exception e) {
			throw new Exception(e);
		}

	}

	@Transactional
	public void atualizaUsuario(Usuario usuario) throws Exception {
		try {
			emf.merge(usuario);
		} catch (NoResultException e) {
			throw new NoResultException();
		} catch (Exception e) {
			throw new Exception(e);
		}

	}

}
