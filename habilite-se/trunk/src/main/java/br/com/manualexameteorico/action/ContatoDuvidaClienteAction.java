package br.com.manualexameteorico.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.manualexameteorico.servico.ServicoEnvioEmail;
import br.com.manualexameteorico.vo.ContatoVO;

import com.opensymphony.xwork2.ActionSupport;

public class ContatoDuvidaClienteAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	
	private ContatoVO contatoVO;
	@Autowired
	private ServicoEnvioEmail servicoEmail;

	@Action(value = "/QuemSomos", results = { @Result(name = "sucesso", location = "/WEB-INF/jsp/empresa/quem_somos.jsp") })
	public String execute() {
		return "sucesso";
	}
	
	@Action(value = "/Contato", results = { @Result(name = "sucesso", location = "/WEB-INF/jsp/empresa/contato.jsp") })
	public String contato() {
		return "sucesso";
	}
	
	@Action(value = "/EnviaContato", results = { @Result(name = "sucesso", location = "/WEB-INF/jsp/empresa/contato.jsp") })
	public String enviaContato() {
		servicoEmail.enviaEmail(contatoVO);
		
		return "sucesso";
	}

	public ContatoVO getContatoVO() {
		return contatoVO;
	}

	public void setContatoVO(ContatoVO contatoVO) {
		this.contatoVO = contatoVO;
	}

}
