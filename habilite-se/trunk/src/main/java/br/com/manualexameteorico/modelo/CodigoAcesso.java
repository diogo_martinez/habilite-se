package br.com.manualexameteorico.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQuery(name = "buscaCodigoAcesso", query = "select ca from CodigoAcesso ca where ca.codigoAcesso = :codigoAcesso and ca.ativo = TRUE")
public class CodigoAcesso {

	@Id
	private Long id;

	@Column(name = "codigo_acesso")
	private String codigoAcesso;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_tipo_usuario")
	private TipoUsuario tipoUsuario;

	private Boolean ativo;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_pessoa_juridica", nullable = false)
	private PessoaJuridica idPessoaJuridica;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoAcesso() {
		return codigoAcesso;
	}

	public void setCodigoAcesso(String codigoAcesso) {
		this.codigoAcesso = codigoAcesso;
	}

	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public PessoaJuridica getIdPessoaJuridica() {
		return idPessoaJuridica;
	}

	public void setIdPessoaJuridica(PessoaJuridica idPessoaJuridica) {
		this.idPessoaJuridica = idPessoaJuridica;
	}

}
