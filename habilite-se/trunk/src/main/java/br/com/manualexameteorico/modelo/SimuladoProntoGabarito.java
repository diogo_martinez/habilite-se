package br.com.manualexameteorico.modelo;

public class SimuladoProntoGabarito {
	
	
	private Integer id;
	
	private SimuladoPronto idSimulado;
	
	private Integer num_pergunta;
	
	private Integer num_resposta;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SimuladoPronto getIdSimulado() {
		return idSimulado;
	}

	public void setIdSimulado(SimuladoPronto idSimulado) {
		this.idSimulado = idSimulado;
	}

	public Integer getNum_pergunta() {
		return num_pergunta;
	}

	public void setNum_pergunta(Integer num_pergunta) {
		this.num_pergunta = num_pergunta;
	}

	public Integer getNum_resposta() {
		return num_resposta;
	}

	public void setNum_resposta(Integer num_resposta) {
		this.num_resposta = num_resposta;
	}

}
