package br.com.manualexameteorico.utilitario;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHA2 {

	private MessageDigest digest;

	private static SHA2 instance;

	private SHA2() {

	}

	public String convertStringToSha256(String valor) {

		try {
			digest = MessageDigest.getInstance("SHA-256");

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static SHA2 getInstance() {
		if (instance == null) {
			instance = new SHA2();
		}
		return instance;
	}
}
