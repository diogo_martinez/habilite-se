package br.com.manualexameteorico.modelo;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "Pergunta_Seq", sequenceName = "Pergunta_codigo_seq", allocationSize = 1)
@NamedQuery(name = "listaTodos", query = "select p from Pergunta p")
public class Pergunta implements Serializable, Comparable<Pergunta> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Pergunta_Seq")
	private Long id;

	private String descricao;

	private String imagem;

	@OneToOne
	@JoinColumn(name = "id_modulo")
	private Modulo idModulo;

	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name = "pergunta_questao",
	joinColumns = {
	@JoinColumn(name="perguntaId") 
	},
	inverseJoinColumns = {
	@JoinColumn(name="questaoId")
	}
	)
	private List<Questao> questoes;

	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_pergunta")
	private Set<Alternativa> alternativas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public Modulo getIdModulo() {
		return idModulo;
	}

	public void setIdModulo(Modulo idModulo) {
		this.idModulo = idModulo;
	}

	

	public List<Questao> getQuestoes() {
		return questoes;
	}

	public void setQuestoes(List<Questao> questoes) {
		this.questoes = questoes;
	}

	@Override
	public int compareTo(Pergunta o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public Set<Alternativa> getAlternativas() {
		return alternativas;
	}

	public void setAlternativas(Set<Alternativa> alternativas) {
		this.alternativas = alternativas;
	}

	

}
