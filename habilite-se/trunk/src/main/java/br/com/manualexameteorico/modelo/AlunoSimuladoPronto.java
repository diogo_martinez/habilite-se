package br.com.manualexameteorico.modelo;

public class AlunoSimuladoPronto {
	
	private Usuario usuario;
	
	private SimuladoPronto simuladoPronto;
	
	private AlunoGabarito alunoGabarito;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public SimuladoPronto getSimuladoPronto() {
		return simuladoPronto;
	}

	public void setSimuladoPronto(SimuladoPronto simuladoPronto) {
		this.simuladoPronto = simuladoPronto;
	}

	public AlunoGabarito getAlunoGabarito() {
		return alunoGabarito;
	}

	public void setAlunoGabarito(AlunoGabarito alunoGabarito) {
		this.alunoGabarito = alunoGabarito;
	}

}
