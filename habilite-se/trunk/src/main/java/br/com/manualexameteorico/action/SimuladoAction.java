package br.com.manualexameteorico.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.manualexameteorico.servico.interfaces.InterfaceServicoSimulado;

import com.opensymphony.xwork2.ActionSupport;

public class SimuladoAction extends ActionSupport {
	private InputStream inputStream;
	private String simuladoPronto;

	@Autowired
	private InterfaceServicoSimulado servicoSimuladoPDF;

	@Action(value = "/GerarSimulado", results = {
			@Result(name = "sucesso", type = "stream", params = {
					"contentType", "document/pdf", "inputName", "inputStream",
					"contentDisposition", "filename=\"document.pdf\"",
					"bufferSize", "1024" }),
			@Result(name = "erro", location = "/jsp/login/erro.jsp"),
			@Result(name = "erroSistema", location = "/WEB-INF/jsp/erro/erroSistema.jsp") })
	public String execute() {
		inputStream = servicoSimuladoPDF.geraSimulado();
		return "sucesso";
	}

	// @Action(value = "/GerarSimulado", results = {
	// @Result(name = "sucesso", type = "jasper", params = {
	// "contentType",
	// "document/pdf","inputName","inputStream","contentDisposition","filename=\"document.pdf\"","bufferSize","1024"
	// }),
	// @Result(name = "erro", location = "/jsp/login/erro.jsp"),
	// @Result(name = "erroSistema", location =
	// "/WEB-INF/jsp/erro/erroSistema.jsp") })
	// public String execute(){
	// inputStream = servicoSimulado.geraSimulado();
	// return "sucesso";
	// }
	@SkipValidation
	@Action(value = "/InicialSimulado", results = {
			@Result(name = "sucesso", location = "/WEB-INF/jsp/simulado/inicial.jsp"),
			@Result(name = "erroSistema", location = "/WEB-INF/jsp/erro/erroSistema.jsp") }, interceptorRefs = {
			@InterceptorRef("sessaoStack"), @InterceptorRef("defaultStack") })
	public String inicial() {

		return "sucesso";

	}

	@SkipValidation
	@Action(value = "/SimuladoPronto", results = {
			@Result(name = "sucesso", type = "stream", params = {
					"contentType", "document/pdf", "inputName", "inputStream",
					"contentDisposition", "filename=\"simulado.pdf\"",
					"bufferSize", "1024" }),
			@Result(name = "erroSistema", location = "/WEB-INF/jsp/erro/erroSistema.jsp") }, interceptorRefs = {
			@InterceptorRef("sessaoStack"), @InterceptorRef("defaultStack") })
	public String simuladoPronto() {
		try {
			File pdf = new File(String.format(
					"/home/habilite/simulados/%s.pdf", simuladoPronto));
			inputStream = new FileInputStream(pdf);
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
		return "sucesso";

	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getSimuladoPronto() {
		return simuladoPronto;
	}

	public void setSimuladoPronto(String simuladoPronto) {
		this.simuladoPronto = simuladoPronto;
	}

}
