package br.com.manualexameteorico.servico.gerenciamemoria;

import java.util.List;

import br.com.manualexameteorico.modelo.Pergunta;

public class GerenciadorAcessoMemoria {
	
	private GerenciadorAcessoMemoria(){
		
	}
	
	private static List<Pergunta> perguntas;
	
	

	public static List<Pergunta> getPerguntas() {
		return perguntas;
	}

	public static void setPerguntas(List<Pergunta> perguntas) {
		GerenciadorAcessoMemoria.perguntas = perguntas;
	}
	
	
	
	
	

}
