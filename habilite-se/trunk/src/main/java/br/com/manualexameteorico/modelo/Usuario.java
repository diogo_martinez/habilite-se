package br.com.manualexameteorico.modelo;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "Usuario_Seq", sequenceName = "Usuario_codigo_seq", allocationSize = 1)
@NamedQuery(name = "consultaLoginSenha", query = "select u from Usuario u where u.usuario=:usuario and u.senha=:senha")
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Usuario_Seq")
	private Long id;

	@Column(nullable = false)
	private String usuario;

	@Column(nullable = false)
	private String senha;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_tipo_usuario", nullable = false)
	private TipoUsuario tipoUsuario;

	@Column(name = "nome_completo", nullable = false)
	private String nomeCompleto;

	@Column(name = "cpf_cnpj", nullable = false)
	private String cpfCnpj;

	@Column(name = "data_nascimento", nullable = false)
	private Calendar dataNascimento;

	@Column(nullable = false)
	private String email;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_codigo_acesso")
	private CodigoAcesso codigoAcesso;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_pessoa_juridica", nullable = false)
	private PessoaJuridica idPessoaJuridica;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public Calendar getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Calendar dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public CodigoAcesso getCodigoAcesso() {
		return codigoAcesso;
	}

	public void setCodigoAcesso(CodigoAcesso codigoAcesso) {
		this.codigoAcesso = codigoAcesso;
	}

	public PessoaJuridica getIdPessoaJuridica() {
		return idPessoaJuridica;
	}

	public void setIdPessoaJuridica(PessoaJuridica idPessoaJuridica) {
		this.idPessoaJuridica = idPessoaJuridica;
	}

}
