package br.com.manualexameteorico.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.manualexameteorico.servico.interfaces.InterfaceServicoUsuario;

import com.opensymphony.xwork2.ActionSupport;

public class CodigoAcessoAction extends ActionSupport{
	
	
	private static final long serialVersionUID = 1L;

	private String codigoAcesso;
	
	@Autowired
	private InterfaceServicoUsuario servicoUsuario;
	
	@Action(value = "/ValidaCodigoAcesso", results = {
			@Result(name = "successo", location = "/WEB-INF/jsp/login/cadastrarLogin.jsp"),
			@Result(name = "input", location = "/WEB-INF/jsp/login/inicial.jsp") })
	public String validaCodigoAcesso() {

		Boolean flagValidacao = servicoUsuario
				.isCodigoAcessoValido(codigoAcesso);
		if (flagValidacao) {
			return "successo";
		} else {
			addActionError(getText("codigo.invalido"));
			return "input";
		}
	}

	public String getCodigoAcesso() {
		return codigoAcesso;
	}

	public void setCodigoAcesso(String codigoAcesso) {
		this.codigoAcesso = codigoAcesso;
	}


}
