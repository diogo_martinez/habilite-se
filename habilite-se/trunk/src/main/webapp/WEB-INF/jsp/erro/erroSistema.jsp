<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Insert title here</title>
<link href="<s:url value="/css/style.css"/>" rel="stylesheet"
	type="text/css" />
</head>
<body>
<div class="NoticeBlocking" id="NoticeBlocking" style="display: none; ">
      <div class="NoticeBlockingOpacity"></div>
    <div class="NoticeContainer" id="NoticeContainer" style="top: 3.5px; position: fixed; left: 338px; ">
      <div class="closeNotification">
		<a class="closeNotificationLink" href="javascript://" onclick="hidePopup();">fechar</a>
	  </div>
	  <div style="height:30px;">
        <div>
          <!--  imagem-->
        </div>
        <div class="NoticeTitle">
          <s:text name="erro.sistema"></s:text>
        </div>
      </div>
      <div class="NoticeText">
			<s:text name="mensagem.erro.sistema"></s:text><s:actionerror />      </div>
    </div>
  </div>
</body>
</html>