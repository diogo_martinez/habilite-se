<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="<s:url value="/css/style.css"/>" rel="stylesheet"
	type="text/css" />
<script src="<s:url value="/js/jquery.js"/>" type="text/javascript"></script>
<script src="<s:url value="/js/jquery.min.js"/>" type="text/javascript"></script>
<script src="<s:url value="/js/cufon-yui.js"/>" type="text/javascript"></script>
<script src="<s:url value="/js/cufon-replace.js"/>"
	type="text/javascript"></script>
<script src="<s:url value="/js/Trajan_Pro_400.font.js"/>"
	type="text/javascript"></script>
<script src="<s:url value="/js/jquery-ui-1.8.18.custom.min.js"/>"
	type="text/javascript"></script>

<script type="text/javascript"
	src="<s:url value="/js/jquery.validate.js"/>"></script>

<script type="text/javascript"
	src="<s:url value="/js/jquery.validate.min.js"/>"></script>

<script type="text/javascript"
	src="<s:url value="/js/jquery.maskedinput-1.3.js"/>"></script>

<script type="text/javascript"
	src="<s:url value="/js/jquery.placeholder-1.1.9.js"/>"></script>
<!--[if lt IE 7]>
	<link href="<s:url value="/css/ie_style.css"/>" rel="stylesheet" type="text/css" />
<![endif]-->
<script type="text/javascript" src="<s:url value="/js/utilitario.js"/>"></script>

<script>
	$(document).ready(function() {

	
		$("#contacts-form2").validate({
			rules : {
				"contatoVO.nome" : {
					required : true,
					minlength : 3
				},
				"contatoVO.email" : {
					required : true,
					email : true
				},
				"contatoVO.mensagem" : {
					required : true,
					minlength : 3
				}				
			}
		});	
	});
</script>


</head>
<body id="page2">
	<div class="header_id">
		<a href="http://www.webdesign.org/website-design">website design</a>
	</div>
	<div id="main">
		<!-- header -->
		
		<%@include file="/jsp/header.jsp"%>
			
    <!-- content -->
    <div id="content">
    	<div class="wrapper">
        <div class="mainContent">
          <div class="section">
            <h2><span>Postal Address</span></h2>
            <!-- .box -->
            <div class="box">
              <div class="img-box">
                <img src="images/6page-img1.gif" alt="" />
                <dl class="address">
                  <dt>Habilite-se.<br />
                  8901 Marmora Road,<br />
                  Glasgow, D04 89GR.</dt>
                 <!--  <dd><span>Freephone:</span>+1 800 559 6580</dd>
                  <dd><span>Telephone:</span>+1 959 603 6035</dd>
                  <dd><span>FAX:</span>+1 504 889 9898</dd> -->
                  <dd class="last">E-mail: <a href="#">atendimento@habilite-se.com.br</a></dd>
                </dl>
              </div>
              <a href="#" class="link1">Ver mapa</a>
            </div>
            <!-- /.box -->
          </div>
          <h2><span>Formulário de Contato</span></h2>
          <!-- .box -->
          <div class="box">
          	<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam non- ummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volut- pat. Ut wisi enim ad minim veniam sed diam ummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim </p>
            <form action="EnviaContato" id="contacts-form2" method="post">
              <fieldset>
                <div class="col-1">
                  <label>Nome:<br />
                    <input type="text" name="contatoVO.nome" value="" />
                  </label>
                  <label>E-mail:<br />
                    <input type="text" name="contatoVO.email" value="" />
                  </label>
                  <label>Telefone:<br />
                    <input type="text" name="contatoVO.telefone" value="" />
                  </label>
                </div>
                <div class="col-2">Mensagem:<br />
                  <textarea cols="" rows="" name="contatoVO.mensagem"></textarea>
                  <div class="wrapper">
                    <a href="#" class="button" onclick="submeter(document.getElementById('contacts-form2'))">Enviar</a><a href="#" class="button" onclick="document.getElementById('contacts-form2').reset()">Limpar</a>
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <div class="aside right">
          <h3><span><span>Our Locations</span></span></h3>
          <!-- .box1 -->
          <div class="box1">
            <ol>
              <li>
                <h6>Brasil</h6>
                Rio de Janeiro.<a href="#" class="arrow">&nbsp;</a>
              </li>
             
            </ol>
          </div>
          <!-- /.box1 -->
        </div>
      </div>
    </div>
    	<%@include file="/jsp/footer.jsp"%>
    </div>
  </div>
  <script type="text/javascript"> Cufon.now(); </script>
</body>
</html>