<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="<s:url value="/css/style.css"/>" rel="stylesheet"
	type="text/css" />
<script src="<s:url value="/js/jquery.js"/>" type="text/javascript"></script>
<script src="<s:url value="/js/jquery.min.js"/>" type="text/javascript"></script>
<script src="<s:url value="/js/cufon-yui.js"/>" type="text/javascript"></script>
<script src="<s:url value="/js/cufon-replace.js"/>"
	type="text/javascript"></script>
<script src="<s:url value="/js/Trajan_Pro_400.font.js"/>"
	type="text/javascript"></script>
<script src="<s:url value="/js/jquery-ui-1.8.18.custom.min.js"/>"
	type="text/javascript"></script>

<script language="javascript" type="text/javascript"
	src="<s:url value="/js/jquery.validate.js"/>"></script>

<script language="javascript" type="text/javascript"
	src="<s:url value="/js/jquery.validate.min.js"/>"></script>

<script language="javascript" type="text/javascript"
	src="<s:url value="/js/jquery.maskedinput-1.3.js"/>"></script>
<script type="text/javascript" src="<s:url value="/js/utilitario.js"/>"></script>

<!--[if lt IE 7]>
	<link href="ie_style.css" rel="stylesheet" type="text/css" />
<![endif]-->

<script>
	$(document).ready(function() {

		$("#contacts-form").validate({
			rules : {
				"email" : {
					required : true,
					email : true
				}
			}
		});
		$("#simulados-prontos-form").validate({
			rules : {
				"simuladoPronto" : {
					required : true
				//	email : true
				}
			}
		});

	});
</script>

</head>

<body id="page6">
	<div id="main">
		<!-- header -->
		<%@include file="/jsp/header.jsp"%>
		<!-- content -->
		<div id="content">
			<div class="wrapper">
				<div class="mainContentSimulado">
					<!--   <div class="section">
            <h2><span>Postal Address</span></h2>
            <!-- .box ->
            <div class="box">
              <div class="img-box">
                <img src="images/6page-img1.gif" alt="" />
                <dl class="address">
                  <dt>The Company Name Inc.<br />
                  8901 Marmora Road,<br />
                  Glasgow, D04 89GR.</dt>
                  <dd><span>Freephone:</span>+1 800 559 6580</dd>
                  <dd><span>Telephone:</span>+1 959 603 6035</dd>
                  <dd><span>FAX:</span>+1 504 889 9898</dd>
                  <dd class="last">E-mail: <a href="#">mail@demolink.org</a></dd>
                </dl>
              </div>
              <a href="#" class="link1">View map</a>
            </div>
            <!-- /.box ->
          </div>-->
					<div class="section">
						<h2>
							<span>Gerador de simulado</span>
						</h2>
						<!-- .box -->
						<div class="box">
							<!-- <p>Selecione os filtros e o formato em que deseja que o
							simulado seja gerado. Os simulados tem 30 questões, com a
							distribuição das matérias de acordo com o site do Detran-RJ.</p> -->
							<p>Em breve teremos filtros e o novos formato para que você
								tenha um estudo mais focado em suas necessidades. Os simulados
								tem 30 questões, com a distribuição das matérias de acordo com o
								site do Detran-RJ.</p>
							<form action="GerarSimulado" id="contacts-form" target="_blank">
								<fieldset>
									<div class="col-1">
										<!-- 	<label>Enter your name:<br /> <input type="text"
										value="" />
									</label> <label>Enter your e-mail:<br /> <input name="email"
										type="text" value="" />
									</label> <label>Enter your fax:<br /> <input type="text"
										value="" />
									</label> -->
									</div>
									<div class="col-2">
										<!-- Enter your message:<br />
									<textarea cols="" rows=""></textarea> -->
										<div class="wrapper">
											<a class="button"
												onclick="submeter(document.getElementById('contacts-form'))">Gerar
												PDF</a>
											<!-- <a
											class="button"
											onclick="document.getElementById('contacts-form').reset()">clear</a> -->
										</div>
									</div>
								</fieldset>
							</form>
						</div>
						<!-- /.box -->
					</div>
					<h2>
						<span>Simulados Prontos</span>
					</h2>
					<!-- .box -->
					<div class="box">
						<!-- <p>Selecione os filtros e o formato em que deseja que o
							simulado seja gerado. Os simulados tem 30 questões, com a
							distribuição das matérias de acordo com o site do Detran-RJ.</p> -->
						<p>Em breve teremos filtros e o novos formato para que você
							tenha um estudo mais focado em suas necessidades. Os simulados
							tem 30 questões, com a distribuição das matérias de acordo com o
							site do Detran-RJ.</p>
						<form action="SimuladoPronto" id="simulados-prontos-form" target="_blank">
							<fieldset>
								<div class="col-1">
									 	<label>Simulado n°:<br /> <select name="simuladoPronto" id="simuladopronto">
									 	<option value="simulado1">Simulado 1</option>
									 	<option value="simulado2">Simulado 2</option>
									 	<option value="simulado3">Simulado 3</option>
									 	<option value="simulado4">Simulado 4</option>
									 	<option value="simulado5">Simulado 5</option>
									 	<option value="simulado6">Simulado 6</option>
									 	<option value="simulado7">Simulado 7</option>
									 	<option value="simulado8">Simulado 8</option>
									 	<option value="simulado9">Simulado 9</option>
									 	<option value="simulado10">Simulado 10</option>									 	
									 	<option value="simulado11">Simulado 11</option>
									 	<option value="simulado12">Simulado 12</option>
									 	<option value="simulado13">Simulado 13</option>
									 	<option value="simulado14">Simulado 14</option>
									 	<option value="simulado15">Simulado 15</option>
									 	<option value="simulado16">Simulado 16</option>
									 	<option value="simulado17">Simulado 17</option>
									 	<option value="simulado18">Simulado 18</option>
									 	<option value="simulado19">Simulado 19</option>
									 	<option value="simulado20">Simulado 20</option>
									 	<option value="simulado21">Simulado 21</option>
									 	<option value="simulado22">Simulado 22</option>
									 	<option value="simulado23">Simulado 23</option>
									 	<option value="simulado24">Simulado 24</option>
									 	<option value="simulado25">Simulado 25</option>
									 	<option value="simulado26">Simulado 26</option>
									 	<option value="simulado27">Simulado 27</option>
									 	<option value="simulado28">Simulado 28</option>
									 	<option value="simulado29">Simulado 29</option>
									 	<option value="simulado30">Simulado 30</option>
									 	<option value="simulado31">Simulado 31</option>
									 	<option value="simulado32">Simulado 32</option>
									 	<option value="simulado33">Simulado 33</option>
									 	<option value="simulado34">Simulado 34</option>
									 	<option value="simulado35">Simulado 35</option>
									 	<option value="simulado36">Simulado 36</option>
									 	<option value="simulado37">Simulado 37</option>
									 	<option value="simulado38">Simulado 38</option>
									 	<option value="simulado39">Simulado 39</option>
									 	<option value="simulado40">Simulado 40</option>
									 	<option value="simulado41">Simulado 41</option>
									 	<option value="simulado42">Simulado 42</option>
									 	<option value="simulado43">Simulado 43</option>
									 	<option value="simulado44">Simulado 44</option>
									 	<option value="simulado45">Simulado 45</option>
									 	<option value="simulado46">Simulado 46</option>
									 	<option value="simulado47">Simulado 47</option>
									 	<option value="simulado48">Simulado 48</option>
									 	<option value="simulado49">Simulado 49</option>
									 	<option value="simulado50">Simulado 50</option>
									 	</select>
									</label> 
								</div>
								<div class="col-2">
									<!-- Enter your message:<br />
									<textarea cols="" rows=""></textarea> -->
									<div class="wrapper">
										<a class="button"
											onclick="submeter(document.getElementById('simulados-prontos-form'))">Download</a>
										<!-- <a
											class="button"
											onclick="document.getElementById('contacts-form').reset()">clear</a> -->
									</div>
								</div>
							</fieldset>
						</form>
					</div>
					<!-- /.box -->
				</div>
				<!-- <div class="aside right">
          <h3><span><span>Our Locations</span></span></h3>
          <!-- .box1 ->
          <div class="box1">
            <ol>
              <li>
                <h6>Argentina</h6>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.<a href="#" class="arrow">&nbsp;</a>
              </li>
              <li>
                <h6>Denmark</h6>
                Set ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.<a href="#" class="arrow">&nbsp;</a>
              </li>
              <li>
                <h6>Finland</h6>
                Ipsum dolor sit amet, con- sectetuer adipiscing elit, sed diam nonummy.<a href="#" class="arrow">&nbsp;</a>
              </li>
              <li>
                <h6>France</h6>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.<a href="#" class="arrow">&nbsp;</a>
              </li>
              <li>
                <h6>Germany</h6>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.<a href="#" class="arrow">&nbsp;</a>
              </li>
              <li>
                <h6>Ireland</h6>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam adipiscing elit, sed diam nonummy.<a href="#" class="arrow">&nbsp;</a>
              </li>
            </ol>
          </div>
          <!-- /.box1 ->
        </div> -->
			</div>
		</div>
		<%@include file="/jsp/footer.jsp"%>
	</div>
	</div>
	<script type="text/javascript">
		Cufon.now();
	</script>
</body>
</html>