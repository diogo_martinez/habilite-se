<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="<s:url value="/css/style.css"/>" rel="stylesheet"
	type="text/css" />
<script src="<s:url value="/js/jquery.js"/>" type="text/javascript"></script>
<script src="<s:url value="/js/jquery.min.js"/>" type="text/javascript"></script>
<script src="<s:url value="/js/cufon-yui.js"/>" type="text/javascript"></script>
<script src="<s:url value="/js/cufon-replace.js"/>"
	type="text/javascript"></script>
<script src="<s:url value="/js/Trajan_Pro_400.font.js"/>"
	type="text/javascript"></script>
<script src="<s:url value="/js/jquery-ui-1.8.18.custom.min.js"/>"
	type="text/javascript"></script>

<script type="text/javascript"
	src="<s:url value="/js/jquery.validate.js"/>"></script>

<script type="text/javascript"
	src="<s:url value="/js/jquery.validate.min.js"/>"></script>

<script type="text/javascript"
	src="<s:url value="/js/jquery.maskedinput-1.3.js"/>"></script>

<script type="text/javascript"
	src="<s:url value="/js/jquery.placeholder-1.1.9.js"/>"></script>
<!--[if lt IE 7]>
	<link href="<s:url value="/css/ie_style.css"/>" rel="stylesheet" type="text/css" />
<![endif]-->
<script type="text/javascript" src="<s:url value="/js/utilitario.js"/>"></script>

<script>
	$(document).ready(function() {

		$(":input[placeholder]").placeholder();

		$("#codigo-acesso-form").validate({
			rules : {
				codigoAcesso : {
					required : true,
					minlength : 3
				}
			}
		});
		$("#login-form").validate({
			rules : {
				"usuario.usuario" : {
					required : true,
					minlength : 3
				},
				"usuario.senha" : {
					required : true,
					minlength : 6
				}
			}
		});
	});
</script>


</head>
<body id="page2">
	<div class="header_id">
		<a href="http://www.webdesign.org/website-design">website design</a>
	</div>
	<div id="main">
		<!-- header -->
		
		<%@include file="/jsp/header.jsp"%>
			
		<!-- content -->
		<div id="content">
			<div class="wrapper">
				<div class="mainContentSimulado">
					<div class="section">
						<h2>
							<span>Passe na prova do Detran-RJ!</span>
						</h2>
						<!-- .box -->
						<div class="box">
							<p>Para quem está iniciando seus estudos para conquistar a
								primeira habilitação ou para os que possuem a carteira há
								muito tempo e precisa renová-la, fica de frente com um grande desafio - a prova de
								legislação, popularmente conhecido como prova teórica.</p>
							<p>Com base em nossas experiências e com pesquisas feitas com
								alunos, colegas e o mecanismo de pesquisas do google, percebemos que a maior parte da prova é
								igual ao simulado disponibilizado pelo órgão público.
								Pensando nisso, desenvolvemos o caderno de questões do exame
								teórico, que reune as questões do simulado em uma apostila que
								você pode estudar a qualquer hora, sem a necessidade de um
								computador com acesso a internet.</p>
							<p>Nós temos certeza que com o caderno de questões do exame
								teórico, você terá muito mais facilidade e tranquilidade para
								passar na prova.</p>
							<a href="#" class="link1">Consulting</a><a href="#" class="link1">Marketing</a><a
								href="#" class="link1">Planning</a>
						</div>
						<!-- /.box -->
					</div>
				<!-- 	<a href="#"><img src="<s:url value="/images/banner.jpg"/>"
						alt="" /></a>
					<h2>
						<span>Business taken to the new heights</span>
					</h2> -->
					<!-- .box ->
					<div class="box">
						<p class="p1">Lorem ipsum dolor sit amet, consectetuer
							adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
							laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim
							veniam.</p>
						<ul>
							<li><a href="#">Lorem ipsum dolor sit amet, consectetuer
									adipiscing elit</a></li>
							<li><a href="#">Sed diam nonummy nibh euismod minim</a></li>
						</ul>
						<a href="#" class="button">read more</a>
					</div>
					<!-- /.box -->
				</div>
				<!--  
				<div class="aside right">

					<!-- .box2 ->
					<div class="section">
						<div class="box2">
							<div class="left-top-corner">
								<div class="right-top-corner">
									<div class="inner">
										<h4>Código de Acesso</h4>
										<s:actionerror />
										<s:form method="POST" action="ValidaCodigoAcesso"
											id="codigo-acesso-form" theme="simple">
											<fieldset>
												<label class="field"><s:textfield cssClass="text"
														name="codigoAcesso"
														placeholder="Digite seu código de acesso"
														onclick="this.value=''" id="codigo_acesso" /></label>
												<div class="wrapper">
													<!-- 	<a href="#" class="fleft cancel">Cancel  subscription</a>->
													<a class="submit"
														onclick="submeter(document.getElementById('codigo-acesso-form'))">Enviar</a>
												</div>
											</fieldset>
										</s:form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.box2 ->

					<div>
						<!-- <h3><span><span>login</span></span></h3> ->
						<!-- .box1 ->

						<div class="box2">
							<div class="left-top-corner">
								<div class="right-top-corner">
									<div class="inner">
										<h4>Login</h4>
										<s:fielderror />
										<s:form action="Login" id="login-form" theme="simple"
											method="POST">
											<fieldset>
												<div class="label">Usuario:</div>
												<label class="field"><input type="text" class="text"
													onclick="this.value=''" name="usuario.usuario" id="usuario" /></label>
												<div class="label">Senha:</div>
												<label class="field"><input type="password"
													class="text" onclick="this.value=''" name="usuario.senha"
													id="senha" /></label>
												<div class="wrapper">
													<!-- 	<a href="#" class="fleft cancel">Cancel  subscription</a>->
													<a class="submit"
														onclick="submeter(document.getElementById('login-form'))">Enviar</a>
												</div>
											</fieldset>
										</s:form>
									</div>
								</div>
							</div>
							<div class="alignright">
								<s:if test=" #session != null && #session.usuario != null">
									<s:url action="IniciaAlteracaoSenha" id="mudarSenha"></s:url>
									<s:a href="%{mudarSenha}" cssClass="link2">Mudar minha senha</s:a>
									<br />
								</s:if>
								<a href="" class="link2">Esqueci a senha</a>
							</div>
							<br />
						</div>

						<!-- /.box1 ->
					</div>
				</div>-->
			</div>
		</div>
			<%@include file="/jsp/footer.jsp"%>
		</div>
	</div>
	<script type="text/javascript">
		Cufon.now();
	</script>
</body>
</html>