<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="<s:url value="/css/style.css"/>" rel="stylesheet" type="text/css" />
<script src="<s:url value="/js/jquery.js"/>" type="text/javascript"></script>
<script src="<s:url value="/js/jquery.min.js"/>" type="text/javascript"></script>
<script src="<s:url value="/js/cufon-yui.js"/>" type="text/javascript"></script>
<script src="<s:url value="/js/cufon-replace.js"/>"
	type="text/javascript"></script>
<script src="<s:url value="/js/Trajan_Pro_400.font.js"/>"
	type="text/javascript"></script>
<script src="<s:url value="/js/jquery-ui-1.8.18.custom.min.js"/>"
	type="text/javascript"></script>

<script language="javascript" type="text/javascript"
	src="<s:url value="/js/jquery.validate.js"/>"></script>

<script language="javascript" type="text/javascript"
	src="<s:url value="/js/jquery.validate.min.js"/>"></script>

<script language="javascript" type="text/javascript"
	src="<s:url value="/js/jquery.maskedinput-1.3.js"/>"></script>
<!--[if lt IE 7]>
	<link href="<s:url value="/css/ie_style.css"/>" rel="stylesheet" type="text/css" />
<![endif]-->
<script type="text/javascript"
	src="<s:url value="/js/utilitario.js"/>"></script>
<script type="text/javascript">
jQuery(function($){
//		$("#cpf_cnpj").mask("999.999.999-99");
		$("#data_nascimento").mask("99/99/9999");		
		});  
</script>
<script>
$(document).ready(function(){	

	$("#contacts-form").validate({
		  rules: {
			  "usuario.nomeCompleto": {
		      required: true,
		      minlength: 3
		    },
		 	 "usuario.cpfCnpj": {
		      required: true,
		      minlength: 11,
		      maxlength: 11,
		      digits: true
		    },
		  	  "usuario.email": {
			   required: true,
			   email: true
			},
			   "usuario.dataNascimento": {
			   required: true			   
			},
			   "usuario.usuario": {
			   required: true,
			   minlength: 3
			},
			   "usuario.senha": {
			   required: true,
			   minlength: 6
			},
			   senha_repetida: {
			   required: true,
			   minlength: 6
			}
			    
		  }
		});

	}); 
  </script>
</head>

<body id="page3">
<div class="header_id"><a href="http://www.webdesign.org/website-design">website design</a></div>  
<div id="main">
    <!-- header -->
    <%@include file="/jsp/header.jsp"%>	
    <!-- content -->
    <div id="content">
      <div class="section">
        <h2><span>Cadastro de login</span></h2>
        <!-- .box -->
        <div class="box">
        <s:actionerror />
          <s:form action="NovoUsuario" method="POST" id="contacts-form" theme="simple">
          <s:hidden name="usuario.codigoAcesso.codigoAcesso" value="%{codigoAcesso}"/>
              <fieldset>
                <div class="col-1">
                  <label>Nome completo:<br />
                    <input type="text" value="" tabindex="1" name="usuario.nomeCompleto" id="nome"/>
                  </label>
                  <label>CPF/CNPJ:<br />
                    <input type="text" value="" tabindex="2" name="usuario.cpfCnpj" id="cpf_cnpj"/>
                  </label>
                  <label>E-mail:<br />
                    <input type="text" value="" tabindex="3" name="usuario.email" id="email"/>
                  </label>
                   <div class="wrapper">
                    <a class="button" onclick="submeter(document.getElementById('contacts-form'))" tabindex="8">Enviar</a><a class="button" onclick="document.getElementById('contacts-form').reset()" tabindex="9">Limpar</a>
                  </div>                  
                  </div>
                  <div class="col-2">
                  <label>Data de nascimento:<br />
                    <input type="text" value="" tabindex="4" name="usuario.dataNascimento" id="data_nascimento"/>
                  </label>
                  <label>Usuario:<br />
                    <input type="text" value="" tabindex="5" name="usuario.usuario" id="usuario"/>
                  </label>
                  <label>Senha:<br />
                    <input type="password" value="" tabindex="6" name="usuario.senha" id="senha"/>
                  </label>
                  <label>Repita a Senha:<br />
                    <input type="password" value="" tabindex="7" id="senha_repetida" name="senha_repetida"/>
                  </label>
                
                 
                </div>
              </fieldset>
            </s:form>
        </div>
        <!-- /.box -->
      </div>
      
    </div>
    	<%@include file="/jsp/footer.jsp"%>
    </div>
  </div>
  <script type="text/javascript"> Cufon.now(); </script>
</body>
</html>