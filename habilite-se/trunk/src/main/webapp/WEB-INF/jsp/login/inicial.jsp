<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="<s:url value="/css/style.css"/>" rel="stylesheet"
	type="text/css" />
<script src="<s:url value="/js/jquery.js"/>" type="text/javascript"></script>
<script src="<s:url value="/js/jquery.min.js"/>" type="text/javascript"></script>
<script src="<s:url value="/js/cufon-yui.js"/>" type="text/javascript"></script>
<script src="<s:url value="/js/cufon-replace.js"/>"
	type="text/javascript"></script>
<script src="<s:url value="/js/Trajan_Pro_400.font.js"/>"
	type="text/javascript"></script>
<script src="<s:url value="/js/jquery-ui-1.8.18.custom.min.js"/>"
	type="text/javascript"></script>

<script type="text/javascript"
	src="<s:url value="/js/jquery.validate.js"/>"></script>

<script type="text/javascript"
	src="<s:url value="/js/jquery.validate.min.js"/>"></script>

<script type="text/javascript"
	src="<s:url value="/js/jquery.maskedinput-1.3.js"/>"></script>

<script type="text/javascript"
	src="<s:url value="/js/jquery.placeholder-1.1.9.js"/>"></script>
<!--[if lt IE 7]>
	<link href="<s:url value="/css/ie_style.css"/>" rel="stylesheet" type="text/css" />
<![endif]-->
<script type="text/javascript" src="<s:url value="/js/utilitario.js"/>"></script>

<script>
	$(document).ready(function() {

		$(":input[placeholder]").placeholder();

		$("#codigo-acesso-form").validate({
			rules : {
				codigoAcesso : {
					required : true,
					minlength : 3
				}
			}
		});
		$("#login-form").validate({
			rules : {
				"usuario.usuario" : {
					required : true,
					minlength : 3
				},
				"usuario.senha" : {
					required : true,
					minlength : 6
				}
			}
		});
	});
</script>


</head>
<body id="page2">
	<div class="header_id">
		<a href="http://www.webdesign.org/website-design">website design</a>
	</div>
	<div id="main">
		<!-- header -->
		
		<%@include file="/jsp/header.jsp"%>	
		
		<!-- content -->
		<div id="content">
			<div class="wrapper">
				<div class="mainContent">
					<div class="section">
						<h2>
							<span>Simulado Impresso</span>
						</h2>
						<!-- .box -->
						<div class="box">
							<p>A área de simulados impresso é voltada para os alunos de
								auto-escolas cadastradas no site Habilite-se. Nela você poderá
								por em prova os conhecimentos adquiridos no Caderno de Questões,
								e se preparar ainda mais para o exame teórico</p>
							<p>Para ter acesso a esta área, basta digitar o codigo de
								acesso impresso em seu Caderno de Questões e criar seu login. Se
								já tiver feito isso, é só digitar seu usuário e senha no
								formulario ao lado, e desfrutar de todo o conteúdo dos
								simulados. Lembre-se, sua auto-escola precisa estar cadastrada
								para a utilização deste serviço.</p>
							<p>Se sua auto-escola não for cadastrada conosco, fale com o
								administrador e peça para que eles se afiliem! O Caderno
								de Questões do Exame Teórico e os nossos simulados on-line juntos,
								formam uma ferramenta poderosíssima para sua aprovação na prova
								do Detran.</p>

						</div>
						<!-- /.box -->
					</div>
				</div>
				<div class="aside right">

					<!-- .box2 -->
					<div class="section">
						<div class="box2">
							<div class="left-top-corner">
								<div class="right-top-corner">
									<div class="inner">
										<h4>Código de Acesso</h4>
										<s:actionerror />
										<s:form method="POST" action="ValidaCodigoAcesso"
											id="codigo-acesso-form" theme="simple">
											<fieldset>
												<label class="field"><s:textfield cssClass="text"
														name="codigoAcesso"
														placeholder="Digite seu código de acesso"
														onclick="this.value=''"  id="codigo_acesso" onkeypress="enterPressionado(event,this.form)"/></label>
												<div class="wrapper">
													<!-- 	<a href="#" class="fleft cancel">Cancel  subscription</a>-->
													<a class="submit"
														onclick="submeter(document.getElementById('codigo-acesso-form'))">Enviar</a>
												</div>
											</fieldset>
										</s:form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.box2 -->

					<div>
						<!-- <h3><span><span>login</span></span></h3> -->
						<!-- .box1 -->

						<div class="box2">
							<div class="left-top-corner">
								<div class="right-top-corner">
									<div class="inner">
										<h4>Login</h4>
										<s:fielderror />
										<s:form action="Login" id="login-form" theme="simple"
											method="POST">
											<fieldset>
												<div class="label">Usuario:</div>
												<label class="field"><input type="text" class="text"
													 name="usuario.usuario" id="usuario" onkeypress="enterPressionado(event,this.form)"/></label>
												<div class="label">Senha:</div>
												<label class="field"><input type="password"
													class="text" name="usuario.senha"
													id="senha" onkeypress="enterPressionado(event,this.form)"/></label>
												<div class="wrapper">
													<!-- 	<a href="#" class="fleft cancel">Cancel  subscription</a>-->
													<a class="submit"
														onclick="submeter(document.getElementById('login-form'))">Enviar</a>
													<!--	<s:submit value="Enviar" cssClass="submit"></s:submit>-->
												</div>
											</fieldset>
										</s:form>
									</div>
								</div>
							</div>
							<div class="alignright">
								<s:if test=" #session != null && #session.usuario != null">
									<s:url action="IniciaAlteracaoSenha" id="mudarSenha"></s:url>
									<s:a href="%{mudarSenha}" cssClass="link2">Mudar minha senha</s:a>
									<br />
								</s:if>
								<a href="" class="link2">Esqueci a senha</a>
							</div>
							<br />
						</div>

						<!-- /.box1 -->
					</div>
				</div>
			</div>
		</div>
			<%@include file="/jsp/footer.jsp"%>
		</div>
	</div>
	<script type="text/javascript">
		Cufon.now();
	</script>
</body>
</html>