<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link href="<s:url value="/css/style.css"/>" rel="stylesheet"
	type="text/css" />
<script src="<s:url value="/js/jquery-1.4.2.min.js"/>"
	type="text/javascript"></script>
<script src="<s:url value="/js/cufon-yui.js"/>" type="text/javascript"></script>
<script src="<s:url value="/js/cufon-replace.js"/>"
	type="text/javascript"></script>
<script src="<s:url value="/js/Trajan_Pro_400.font.js"/>"
	type="text/javascript"></script>
<script type="text/javascript" src="<s:url value="/js/utilitario.js"/>"></script>
<!--[if lt IE 7]>
	<link href="<s:url value="/css/ie_style.css"/>" rel="stylesheet" type="text/css" />
<![endif]-->
</head>

<body id="page3">
	<div class="header_id">
		<a href="http://www.webdesign.org/website-design">website design</a>
	</div>
	<div id="main">
		<!-- header -->
		<%@include file="/jsp/header.jsp"%>	
		<!-- content -->
		<div id="content">
			<div class="section">
				<h2>
					<span>Alterar Senha</span>
				</h2>
				<!-- .box -->
				<div class="box">
					<s:form action="AlteraSenha" method="POST" id="contacts-form"
						theme="simple">

						<fieldset>
							<div class="col-1">
								<label>Usuario:<br /> <input type="text"
									value="<s:property value="#session.usuario.usuario" />"
									name="alteraSenhaVO.usuario" id="usuario" />
								</label> <label>Senha Antiga:<br /> <input type="password"
									value="" name="alteraSenhaVO.senhaAntiga" id="senha_antiga" />
								</label> <label>Senha Nova:<br /> <input type="password"
									value="" name="alteraSenhaVO.novaSenha" id="nova_senha" />
								</label> <label>Repita Nova Senha:<br /> <input type="password"
									value="" name="alteraSenhaVO.novaSenhaRepetida"
									id="nova_senha_repetida" />
								</label>
								<div class="wrapper">
									<a href="#" class="button"
										onclick="document.getElementById('contacts-form').submit()">Enviar</a><a
										href="#" class="button"
										onclick="document.getElementById('contacts-form').reset()">Limpar</a>
								</div>
							</div>
						</fieldset>
					</s:form>
				</div>
				<!-- /.box -->
			</div>

		</div>
			<%@include file="/jsp/footer.jsp"%>
		</div>
	</div>
	<script type="text/javascript">
		Cufon.now();
	</script>
</body>
</html>