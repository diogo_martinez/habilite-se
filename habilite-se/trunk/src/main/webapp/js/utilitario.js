function submeter(form){
	var resp = $(form).valid();
	
	if(resp){
		form.submit();
	}
}
function showPopup(){  document.getElementById('NoticeBlocking').style.display="block";}
function hidePopup(){  document.getElementById('NoticeBlocking').style.display="none";} 
//JavaScript Document
//adiciona mascara de cnpj
function MascaraCNPJ(cnpj){
      if(mascaraInteiro(cnpj)==false){
              event.returnValue = false;
      }       
      return formataCampo(cnpj, '00.000.000/0000-00', event);
}

//adiciona mascara de cep
function MascaraCep(cep){
              if(mascaraInteiro(cep)==false){
              event.returnValue = false;
      }       
      return formataCampo(cep, '00.000-000', event);
}

//adiciona mascara de data
function MascaraData(data){
      if(mascaraInteiro(data)==false){
              event.returnValue = false;
      }       
      return formataCampo(data, '00/00/0000', event);
}

//adiciona mascara ao telefone
function MascaraTelefone(tel){  
      if(mascaraInteiro(tel)==false){
              event.returnValue = false;
      }       
      return formataCampo(tel, '(00) 0000-0000', event);
}

//adiciona mascara ao CPF
function MascaraCPF(cpf){
      if(mascaraInteiro(cpf)==false){
              event.returnValue = false;
      }       
      return formataCampo(cpf, '000.000.000-00', event);
}

//valida telefone
function ValidaTelefone(tel){
      exp = /\(\d{2}\)\ \d{4}\-\d{4}/
      if(!exp.test(tel.value))
              alert('Numero de Telefone Invalido!');
}

//valida CEP
function ValidaCep(cep){
      exp = /\d{2}\.\d{3}\-\d{3}/
      if(!exp.test(cep.value))
              alert('Numero de Cep Invalido!');               
}

//valida data
function ValidaData(data){
      exp = /\d{2}\/\d{2}\/\d{4}/
      if(!exp.test(data.value))
              alert('Data Invalida!');                        
}

//valida o CPF digitado
function validaCPF(cpf) {
	var exp = /\.|\-/g;
      cpf = cpf.value.toString().replace( exp, "" );
      erro = new String;
      if (cpf.length < 11) erro += "Sao necessarios 11 digitos para verificacao do CPF! \n\n"; 
      var nonNumbers = /\D/;
      if (nonNumbers.test(cpf)) erro += "A verificacao de CPF suporta apenas numeros! \n\n"; 
      if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999"){
              erro += "Numero de CPF invalido!"
    }
    var a = [];
    var b = new Number;
    var c = 11;
    for (i=0; i<11; i++){
            a[i] = cpf.charAt(i);
            if (i < 9) b += (a[i] * --c);
    }
    if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
    b = 0;
    c = 11;
    for (y=0; y<10; y++) b += (a[y] * c--); 
    if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
    if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10])){
            erro +="<ul><li>Digito verificador com problema!</li></ul>";
    }
    if (erro.length > 0){
        //    alert(erro);
  	  document.getElementById("mensagem").innerHTML = erro;
            return false;
    }
    return true;
}

//valida numero inteiro com mascara
function mascaraInteiro(){
      if (event.keyCode < 48 || event.keyCode > 57){
              event.returnValue = false;
              return false;
      }
      return true;
}

//valida o CNPJ digitado
function ValidarCNPJ(ObjCnpj){
      var cnpj = ObjCnpj.value;
      var valida = new Array(6,5,4,3,2,9,8,7,6,5,4,3,2);
      var dig1= new Number;
      var dig2= new Number;
      
      exp = /\.|\-|\//g
      cnpj = cnpj.toString().replace( exp, "" ); 
      var digito = new Number(eval(cnpj.charAt(12)+cnpj.charAt(13)));
              
      for(i = 0; i<valida.length; i++){
              dig1 += (i>0? (cnpj.charAt(i-1)*valida[i]):0);  
              dig2 += cnpj.charAt(i)*valida[i];       
      }
      dig1 = (((dig1%11)<2)? 0:(11-(dig1%11)));
      dig2 = (((dig2%11)<2)? 0:(11-(dig2%11)));
      
      if(((dig1*10)+dig2) != digito)  
              alert('CNPJ Invalido!');
              
}

//formata de forma generica os campos
function formataCampo(campo, Mascara, evento) { 
      var boleanoMascara; 
      
      var Digitato = evento.keyCode;
      exp = /\-|\.|\/|\(|\)| /g
      campoSoNumeros = campo.value.toString().replace( exp, "" ); 
 
      var posicaoCampo = 0;    
      var NovoValorCampo="";
      var TamanhoMascara = campoSoNumeros.length;; 
      
      if (Digitato != 8) { // backspace 
              for(i=0; i<= TamanhoMascara; i++) { 
                      boleanoMascara  = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".")
                                                              || (Mascara.charAt(i) == "/")) 
                      boleanoMascara  = boleanoMascara || ((Mascara.charAt(i) == "(") 
                                                              || (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " ")) 
                      if (boleanoMascara) { 
                              NovoValorCampo += Mascara.charAt(i); 
                                TamanhoMascara++;
                      }else { 
                              NovoValorCampo += campoSoNumeros.charAt(posicaoCampo); 
                              posicaoCampo++; 
                        }              
                }      
              campo.value = NovoValorCampo;
                return true; 
      }else { 
              return true; 
      }
     
}
function enterPressionado(event,form){
	  if(event.keyCode=='13'){
		  submeter(form);
		  }

}