package jmeter.detran.filtro;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class QuestionarioFilter extends Thread {

	private final String url = "c:\\apostila\\";
	private final String url2 = "c:\\apostila\\filtrado\\";
	private List<String> arquivos = null;

	public void run() {
		FileWriter writer = null;
		PrintWriter print = null;
		BufferedReader reader = null;
		for (String arquivo : arquivos) {
			File file = new File(url + arquivo);
			File file2 = new File(url2 + arquivo);
			try {

				writer = new FileWriter(file2, true);
				print = new PrintWriter(writer);
				reader = new BufferedReader(new FileReader(file));

				String linha = null;
				String linha2 = null;

				while ((linha = reader.readLine()) != null) {
					boolean igual = false;
					BufferedReader reader2 = new BufferedReader(new FileReader(
							file2));

					while ((linha2 = reader2.readLine()) != null) {

						int indexImg = linha.indexOf("<span>");
						// int indexDe = linha
						// .indexOf("id=\"descricao-questao\">");
						int indexPara = linha
								.indexOf("<ul class=\"lista-correcao");
						if ((indexImg != -1 && indexPara != -1)
								&& linha2.contains(linha.substring(indexImg,
										indexPara))) {
							igual = true;
							break;
						} // else if ((indexDe != -1 && indexPara != -1)
							// && linha2.contains(linha.substring(indexDe,
							// indexPara))) {
							// igual = true;
							// break;
							// }

					}
					if (!igual && linha.length() > 10) {
						print.println(linha);
						print.flush();
						System.out.println("Nova linha:" + linha);
					} else {
						// System.out.println("linha1: " + linha + "   linha2: "
						// + linha2);
					}
					reader2.close();

				}

			} catch (FileNotFoundException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		try {
			reader.close();
			print.close();
			writer.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

		System.out.println("terminou");

	}

	public QuestionarioFilter() {

	}

	public QuestionarioFilter(List<String> arquivos) {
		this.arquivos = arquivos;
	}

	public static void main(String args[]) {
		List<String> lista1 = new ArrayList<String>();
		lista1.add("questao1.txt");
		// lista1.add("questao2.txt");
		// lista1.add("questao3.txt");
		// lista1.add("questao4.txt");
		// lista1.add("questao5.txt");
		// lista1.add("questao6.txt");
		// lista1.add("questao7.txt");
		// lista1.add("questao8.txt");
		// lista1.add("questao9.txt");
		// lista1.add("questao10.txt");
		// lista1.add("questao11.txt");
		// lista1.add("questao12.txt");
		// lista1.add("questao13.txt");
		// lista1.add("questao14.txt");
		// lista1.add("questao15.txt");
		//
		// List<String> lista2 = new ArrayList<String>();
		// lista2.add("questao16.txt");
		// lista2.add("questao17.txt");
		// lista2.add("questao18.txt");
		// lista2.add("questao19.txt");
		// lista2.add("questao20.txt");
		// lista2.add("questao21.txt");
		// lista2.add("questao22.txt");
		// lista2.add("questao23.txt");
		// lista2.add("questao24.txt");
		// lista2.add("questao25.txt");
		// lista2.add("questao26.txt");
		// lista2.add("questao27.txt");
		// lista2.add("questao28.txt");
		// lista2.add("questao29.txt");
		// lista2.add("questao30.txt");

		QuestionarioFilter q1 = new QuestionarioFilter(lista1);

		// QuestionarioFilter q2 = new QuestionarioFilter(lista2);

		Thread t1 = new Thread(q1);

		// Thread t2 = new Thread(q2);

		t1.start();
		// t2.start();

	}

}
